const express = require('express')
const bodyParser = require('body-parser') // importando o body parser para o programa
const app = express()
//seta uma configuração para o body 
app.use(bodyParser.urlencoded({extended:true})) // conversão
app.use(bodyParser.json()) // dizendo que bodyParser vai usar json
const port = 8000 // não significa segundos. Isso é o código da porta

// o body parser valida os dados de requisição via body
app.listen(port,() => {
    console.log("Projeto executando na porta: " + port)
})

app.get('/aluno',(req, res) => {
    res.send("{message:aluno encontrado}")
})

// recurso de request.query
app.get('/aluno/filtros',(req, res) => {
    let source = req.query // request da query
    let ret = `Dados solicitados: ${source.nome} ${source.sobrenome}`
    res.send(`{message:${ret}}`) // response
})

// recurso de request.param
app.get('/aluno/pesquisa/:valor', (req, res) => {
    console.log("Entrou")
    let dado = req.params.valor
    let ret = `Dados solicitados: ${dado}`
    res.send(`{message: ${ret}}`)
})

// recurso de request.body
app.post('/aluno', (req, res) => { // indica que vai gravar os dados por causa do post
    let dados = req.body
    let ret = `Dados enviados: Nome: ${dados.nome} `
    ret += `Sobrenome: ${dados.sobrenome} `
    ret += `Idade: ${dados.idade} `
    res.send(`{message: ${ret}}`)
})